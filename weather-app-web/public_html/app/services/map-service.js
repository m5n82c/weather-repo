angular.module('weather.app').factory('MapService', ['$http', '$cookies', function ($http, $cookies) {

        var geocoder = new google.maps.Geocoder();

        var factory = {
            onLatLngResolve: function (latLng, onResolved, onFailed) {
                geocoder.geocode({
                    'latLng': latLng
                }, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            var location = {
                                position: {
                                    lat: results[0].geometry.location.lat,
                                    lng: results[0].geometry.location.lng
                                },
                                name: factory.onCityResolve(results[0])
                            }
                            onResolved(location);
                        } else {
                            onFailed('no-results-found');
                        }
                    } else {
                        onFailed('geocoder-failed');
                    }
                });

            },
            onCityResolve: function (geoData) {
                var found = false;
                var i = 0;
                while (i < geoData.address_components.length && !found) {
                    var types = geoData.address_components[i].types;
                    found = types.indexOf('political') > -1 && (types.indexOf('locality') > -1 || types.indexOf('administrative_area_level_1') > -1);
                    ++i;
                }
                if (found) {
                    return geoData.address_components[i - 1].long_name;
                }
                return undefined;
            },
            checkPosition: function (success, error) {
                var cookieLongitude = $cookies.get('longitude'),
                        cookieLatitude = $cookies.get('latitude');
                
                if (angular.isDefined(cookieLongitude) && angular.isDefined(cookieLatitude)) {
                    factory.onLatLngResolve(new google.maps.LatLng(cookieLatitude, cookieLongitude), success, error);
                } else {
                    navigator.geolocation.getCurrentPosition(function (geoposition) {
                        factory.onLatLngResolve(
                                new google.maps.LatLng(geoposition.coords.latitude, geoposition.coords.longitude), 
                                success, error);
                    }, error);
                }
            }

        };

        return factory;

    }]);
