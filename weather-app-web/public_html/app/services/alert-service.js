angular.module('weather.app').factory('AlertService', ['$http', function ($http) {

    return {

        ping: function () {
            return $http({
                method: 'GET',
                url: 'rest/weather/ping'
            });
        },

        createAlert: function (alert) {
            return $http({
                method: 'POST',
                url: 'rest/alert',
                data: alert
            });
        },

        editAlert: function (alert) {
            return $http({
                method: 'PUT',
                url: 'rest/alert/'+alert.id,
                data: alert
            });
        },

        checkAlert: function (alert) {
            return $http({
                method: 'POST',
                url: 'rest/alert/check-alert',
                data: alert
            });
        },

        checkAndReturnAlert: function (alert) {
            return $http({
                method: 'POST',
                url: 'rest/alert/check-and-return-alert',
                data: alert
            });
        },

        resolveTemperatureByAlertId: function (alertId) {
            return $http({
                method: 'GET',
                url: 'rest/alert/' + alertId + '/temperature'
            })
        }

    };

}]);
