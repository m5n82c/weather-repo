angular.module('weather.app').factory('AlertResolverService', ['$q', 'AlertService',
    function ($q, AlertService) {
        return {

            resolveTemperatureByAlertId: function (alertId) {
                var deferred = $q.defer();
                AlertService.resolveTemperatureByAlertId(alertId)
                    .success(function (object) {
                        deferred.resolve(object);
                    }).error(function (error) {
                        deferred.reject(error);
                    });
                return deferred.promise;
            }
        }
}]);
