angular.module('weather.app').controller('MainController', ['$scope', '$cookies', '$uibModal', '$timeout', '$log', 'MapService', 'AlertService', function ($scope, $cookies, $uibModal, $timeout, $log, MapService, AlertService) {

        $scope.alert = {};
        $scope.config = {
            saved: false
        };

        $scope.$watch('alert.email', function () {
            checkEmailAndCity();
        });

        $scope.$watch('alert.city', function () {
            checkEmailAndCity();
        });

        function checkEmailAndCity() {
            if ($scope.alert.city && $scope.alert.email) {
                AlertService.checkAndReturnAlert($scope.alert).success(function (response) {
                    if (response.taken) {
                        $scope.alert.id = response.alert.id;
                    } else {
                        $scope.alert.id = undefined;
                    }
                }).error(function (reason) {
                    $log.error(reason);
                });
            }
        }

        $scope.submitAlert = function () {
            if (!$scope.alert.id) {
                $scope.createAlert($scope.alert);
            } else {
                $scope.editAlert($scope.alert);
            }
        };

        $scope.createAlert = function () {
            AlertService.createAlert($scope.alert).success(function (response) {
                $log.info(response);
                $scope.alert = response;
                $scope.config.saved = true;
                $timeout(function () {
                    $scope.config.saved = false;
                }, 3000);
            }).error(function (reason) {
                $log.error(reason);
            });
        };

        $scope.editAlert = function () {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/templates/confirm-modal.html',
                controller: 'ConfirmController',
                size: 'sm',
                resolve: {
                    'alert': function () {
                        return $scope.alert;
                    },
                    'oldTemperature': ['AlertResolverService', function (AlertResolverService) {
                            return AlertResolverService.resolveTemperatureByAlertId($scope.alert.id);
                        }]
                }
            });

            modalInstance.result.then(function () {
                AlertService.editAlert($scope.alert).success(function (response) {
                    $log.info(response);
                    $scope.alert = response;
                    $scope.config.saved = true;
                    $timeout(function () {
                        $scope.config.saved = false;
                    }, 3000);
                }).error(function (reason) {
                    $log.error(reason);
                });
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });

        };

        $scope.map = {
            center: {
                latitude: 60.09,
                longitude: 6.86
            },
            zoom: 3,
            options: {
                mapTypeControlOptions: {
                    mapTypeIds: []
                },
                streetViewControl: false
            },
            markers: [],
            events: {
                click: function (map, eventName, originalEventArgs) {
                    var position = originalEventArgs[0].latLng;

                    MapService.onLatLngResolve(
                            position,
                            function (location) {
                                $scope.alert.city = location.name;
                                $scope.setMark(position);
                                $scope.$apply();
                            },
                            function (reason) {
                                $log.error('failed: ' + reason);
                                $scope.alert.city = undefined;
                            });
                }
            }
        };

        $scope.searchbox = {
            template: 'app/templates/search-box.html',
            position: 'LEFT_TOP',
            options: {
                autocomplete: true,
                types: ['(cities)']
                        //componentRestrictions: {
                        // country: 'fr'
                        //}
            },
            events: {
                place_changed: function (autocomplete) {
                    var place = autocomplete.getPlace();

                    $scope.alert.city = MapService.onCityResolve(place);
                    $scope.setMark(place.geometry.location);
                }
            }
        };

        $scope.setMark = function (position) {
            $scope.map.markers[0] = {
                id: Date.now(),
                coords: {
                    latitude: position.lat(),
                    longitude: position.lng()
                }
            };
            $scope.map.center.latitude = position.lat();
            $scope.map.center.longitude = position.lng();
        };

        // Check user position

        function onSuccess(location) {
            if (location) {
                $scope.$evalAsync(function () {
                    $scope.alert.city = location.name;
                    $scope.setMark(location.position);
                    $scope.map.zoom = 10;

                    $cookies.put('latitude', location.position.lat());
                    $cookies.put('longitude', location.position.lng());
                }, 1);
            }
        }
        ;

        function onError(error) {
            $log.error('error: ' + error);
            $scope.setCenter(60.06, 6.86);
            $scope.map.zoom = 3;
        }
        ;


        $scope.checkUserPosition = function () {
            MapService.checkPosition(onSuccess, onError);
        };

    }]);
