angular.module('weather.app').controller('ConfirmController', ['$scope', '$uibModalInstance', 'alert', 'oldTemperature', function ($scope, $uibModalInstance, alert, oldTemperature) {


    $scope.alert = alert;
    $scope.oldTemperature = oldTemperature;

    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };


}]);
