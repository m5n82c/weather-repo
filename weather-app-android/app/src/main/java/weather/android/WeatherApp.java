package weather.android;

import android.app.Application;

import weather.android.http.core.HttpAction;

/**
 * Created by Admin on 2016.02.08..
 */
public class WeatherApp extends Application {

    @Override
    public void onCreate(){
        super.onCreate();
        HttpAction.setBaseUrl("http://198.199.127.60:8080/weather-app-war/rest");
    }

}
