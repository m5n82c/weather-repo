package weather.android;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class MapsActivity extends FragmentActivity implements
        OnMapReadyCallback {

    public static final String INVALID_LOCATION = "Invalid location";

    private TextView locationView;
    private Button saveButton;
    private Button trackButton;

    private DialogOnClickListener dialogOnClickListener = new DialogOnClickListener(MapsActivity.this);

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationView = (TextView) findViewById(R.id.location);
        saveButton = (Button) findViewById(R.id.save);
        trackButton = (Button) findViewById(R.id.track);

        saveButton.setOnClickListener(dialogOnClickListener);

        trackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);

                boolean enabled = service
                        .isProviderEnabled(LocationManager.GPS_PROVIDER);

                if (!enabled) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }

                if (service != null) {
                    if (ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                            || ContextCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        Location location = service.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null){
                            addMarker(location.getLatitude(), location.getLongitude());
                        } else {
                            Toast.makeText(MapsActivity.this, "Your current location is unkown.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                addMarker(latLng);
            }
        });
    }

    public void addMarker(double latitude, double longitude){
        addMarker(new LatLng(latitude, longitude));
    }

    public void addMarker(LatLng latLng){
        try {
        Geocoder geocoder = new Geocoder(MapsActivity.this, Locale.getDefault());
        List<Address> addresses = null;

        addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);

        if (addresses.size() > 0){
            String title = addresses.get(0).getLocality();
            if (title == null){
                title = INVALID_LOCATION;
            }
            locationView.setText(title);
            dialogOnClickListener.setCity(title);
            mMap.clear();
            mMap.addMarker(new MarkerOptions().position(latLng).title(title));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            saveButton.setEnabled(true);
        } else {
            locationView.setText(INVALID_LOCATION);
            saveButton.setEnabled(false);
        }
    } catch (IOException e) {
        e.printStackTrace();
    }
    }
}
