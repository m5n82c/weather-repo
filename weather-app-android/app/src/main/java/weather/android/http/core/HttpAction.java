package weather.android.http.core;

import android.os.AsyncTask;

import weather.android.http.response.IResponseHandler;
import weather.android.http.core.url.Url;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class HttpAction<R> extends AsyncTask<Void, Void, R> {

    protected static final Logger LOGGER = Logger.getLogger(HttpAction.class.getName());
    public static final String UTF_8 = "utf-8";
    private static String BASE_URL;

    private final ObjectMapper mapper;
    private HttpClient httpClient;
    private Throwable errorStack;
    protected HttpRequestBase httpRequestBase;
    private Map<String, String> headers;
    protected Url actionUrl;

    protected abstract IResponseHandler<R> getResponseHandler();
    protected abstract HttpRequestBase createHttpRequestBase();
    protected abstract TypeReference getTypeReference();
    protected abstract Url setUrl(Url defaultUrl);

    protected HttpAction() {
        mapper = new ObjectMapper();
    }

    @Override
    protected void onPreExecute() {
        actionUrl = setUrl(new Url(BASE_URL));
        LOGGER.log(Level.INFO, "Url: " + actionUrl.getFullPath());
        httpRequestBase = createHttpRequestBase();
        headers = buildHeaders();
        addHeadersToRequest();
    }

    @Override
    protected R doInBackground(Void... params) {
        try {
            return convertResult(getResponse());
        } catch (Throwable t){
            LOGGER.log(Level.SEVERE, "Exception in http action", t);
            errorStack = t;
            return null;
        }
    }

    @Override
    protected void onPostExecute(R result) {
        IResponseHandler<R> responseHandler = getResponseHandler();
        if (responseHandler != null) {
            if (null == errorStack) {
                getResponseHandler().reportResult(result);
            } else {
                getResponseHandler().reportAlert(errorStack.getMessage());
            }
        }
    }

    private HttpClient getHttpClient() {
        if (httpClient == null) {
            httpClient = new DefaultHttpClient();
        }
        return httpClient;
    }

    public String getResponse() throws IOException {
        HttpEntity entity = getHttpClient().execute(httpRequestBase).getEntity();
        if (entity != null) {
            return convertStreamToString(entity.getContent());
        }
        return null;
    }

    protected R convertResult(String json) throws IOException {
        if (json != null && json.startsWith("<!DOCTYPE")){
            throw new IllegalStateException(json);
            //return null;
        }
        return mapper.readValue(json, getTypeReference());
    }

    private static  String convertStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
                reader.readLine();
            }
        } finally {
            if ( null != reader ) {
                reader.close();
            }
        }
        return sb.toString();
    }

    protected Map<String, String> buildHeaders(){
        Map<String, String> result = new HashMap<String, String>();
        result.put("Accept", "application/json");
        return result;
    }

    private final void addHeadersToRequest(){
        for(Map.Entry<String, String> headerEntry:  headers.entrySet()){
            httpRequestBase.addHeader(headerEntry.getKey(), headerEntry.getValue());
        }
    }

    public static void setBaseUrl(String baseUrl){
        BASE_URL = baseUrl;
    }

    protected ObjectMapper getMapper() {
        return mapper;
    }

}
