package weather.android.http;

import com.fasterxml.jackson.core.type.TypeReference;

import weather.android.http.core.action.PutAction;
import weather.android.http.response.IResponseHandler;
import weather.android.http.core.url.Url;
import weather.android.model.Alert;

/**
 * Created by Admin on 2016.02.08..
 */
public class PutAlertRequest extends PutAction<Alert, Alert> {

    private Alert alert;

    public PutAlertRequest(Alert alert, IResponseHandler<Alert> responseHandler){
        super(responseHandler);
        this.alert = alert;
    }

    @Override
    protected Alert getPayload() {
        return alert;
    }

    @Override
    protected TypeReference getTypeReference() {
        return new TypeReference<Alert>(){

        };
    }

    @Override
    protected Url setUrl(Url defaultUrl) {
        return defaultUrl.setPath("/alert/"+alert.getId());
    }
}
