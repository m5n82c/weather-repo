package weather.android.http.core.action;

import weather.android.http.core.HttpSupplier;
import weather.android.http.response.IResponseHandler;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;

import java.util.Map;

public abstract class PostAction<P, R> extends HttpSupplier<P, R> {

    public PostAction(IResponseHandler<R> responseHandler){
        super(responseHandler);
    }

    @Override
    protected final HttpRequestBase createHttpRequestBase() {
        HttpPost httpPost = new HttpPost(actionUrl.getFullPath());
        httpPost.setEntity(getHttpEntity());
        return httpPost;
    }

    @Override
    protected Map<String, String> buildHeaders() {
        Map<String, String> result =  super.buildHeaders();
        result.put("Content-type", "application/json; charset=UTF-8");
        return result;
    }
}
