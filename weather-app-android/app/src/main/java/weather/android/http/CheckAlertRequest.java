package weather.android.http;

import com.fasterxml.jackson.core.type.TypeReference;

import weather.android.http.core.action.PostAction;
import weather.android.http.response.IResponseHandler;
import weather.android.http.core.url.Url;
import weather.android.model.Alert;
import weather.android.model.CheckAlertDTO;

/**
 * Created by Admin on 2016.02.08..
 */
public class CheckAlertRequest extends PostAction<Alert, CheckAlertDTO>{

    private Alert alert;

    public CheckAlertRequest(Alert alert, IResponseHandler<CheckAlertDTO> responseHandler){
        super(responseHandler);
        this.alert = alert;
    }

    @Override
    protected TypeReference getTypeReference() {
        return new TypeReference<CheckAlertDTO>(){

        };
    }

    @Override
    protected Url setUrl(Url defaultUrl) {
        return defaultUrl.setPath("/alert/check-and-return-alert");
    }

    @Override
    protected Alert getPayload() {
        return alert;
    }
}
