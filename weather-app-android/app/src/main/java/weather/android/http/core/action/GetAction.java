package weather.android.http.core.action;

import weather.android.http.core.HttpAction;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;


public abstract class GetAction<R> extends HttpAction<R> {


    @Override
    protected final HttpRequestBase createHttpRequestBase() {
        return new HttpGet(actionUrl.getFullPath());
    }
}
