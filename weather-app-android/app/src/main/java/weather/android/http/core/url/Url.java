package weather.android.http.core.url;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Url {

    private static final String QUERY_SEPARATOR = "&";
    private static final String QUERY_EQUALS = "=";

    private String baseUrl;
    private String path;
    private Map<String, Object> queryParams;
    private String fullPath;

    public Url(String baseUrl){
        queryParams = new HashMap<String, Object>();
        this.baseUrl = baseUrl;
    }

    public Url setPath(String path){
        this.path=path;
        return this;
    }

    public Url addQueryParam(String key, String value){
        queryParams.put(key, value);
        return this;
    }

    public Url addQueryParam(String key, List<String> values){
        queryParams.put(key, values);
        return this;
    }

    public String getFullPath(){
        if(null == fullPath){
            fullPath = buildFullPath();
        }
        return fullPath;
    }

    private String buildFullPath(){
        StringBuilder res = new StringBuilder();
        res.append(baseUrl).append(path);
        if (!queryParams.isEmpty()) {
            String sep = "";
            res.append("?");
            for(Map.Entry<String, Object> entry: queryParams.entrySet()){
                res.append(sep);
                sep = QUERY_SEPARATOR;
                String key = entry.getKey();
                Object value = entry.getValue();
                if(value instanceof String){
                    res.append(key).append(QUERY_EQUALS).append(value.toString());
                }else {
                    List<String> queryParamList = (List<String>) value;
                    String insideQuerySep = "";
                    for(String listValue: queryParamList){
                        res.append(insideQuerySep).append(key).append(QUERY_EQUALS).append(listValue);
                        insideQuerySep = QUERY_SEPARATOR;
                    }
                }
            }
        }
        return res.toString();
    }
}
