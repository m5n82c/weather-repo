package weather.android.http.core.action;

import weather.android.http.core.HttpSupplier;
import weather.android.http.response.IResponseHandler;

import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;

import java.util.Map;

public abstract class PutAction<P, R> extends HttpSupplier<P, R> {

    public PutAction(IResponseHandler<R> responseHandler) {
        super(responseHandler);
    }

    @Override
    protected final HttpRequestBase createHttpRequestBase() {
        HttpPut httpPut = new HttpPut(actionUrl.getFullPath());
        httpPut.setEntity(getHttpEntity());
        return httpPut;
    }

    @Override
    protected Map<String, String> buildHeaders() {
        Map<String, String> result =  super.buildHeaders();
        result.put("Content-type", "application/json; charset=UTF-8");
        return result;
    }

}
