package weather.android.http.core.action;

import weather.android.http.core.HttpAction;

import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpRequestBase;

public abstract class DeleteAction<R> extends HttpAction<R> {

    @Override
    protected final HttpRequestBase createHttpRequestBase() {
        HttpDelete httpDelete = new HttpDelete(actionUrl.getFullPath());
        return httpDelete;
    }
}
