package weather.android.http;

import android.app.Dialog;
import android.widget.Toast;

import weather.android.http.response.IResponseHandler;
import weather.android.model.Alert;

/**
 * Created by Admin on 2016.02.08..
 */
public class AlertResponseHandler implements IResponseHandler<Alert> {

    private Dialog dialog;

    public AlertResponseHandler(Dialog dialog) {
        this.dialog = dialog;
    }

    @Override
    public void reportAlert(String error) {
        Toast.makeText(dialog.getContext(), "Something went wrong. Try again later.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void reportResult(Alert result) {
        Toast.makeText(dialog.getContext(), "Saved", Toast.LENGTH_SHORT).show();
        dialog.dismiss();
    }

}
