package weather.android.http.response;

public interface IResponseHandler<T> {

    void reportAlert(String error);

    void reportResult(T result);

}
