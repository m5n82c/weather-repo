package weather.android.http.core;

import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.StringEntity;

import java.util.logging.Level;

import weather.android.http.response.IResponseHandler;


public abstract class HttpSupplier<P, R> extends HttpAction<R> {

    private IResponseHandler<R> responseHandler;

    public HttpSupplier(IResponseHandler<R> responseHandler){
        this.responseHandler = responseHandler;
    }

    protected abstract P getPayload(); // object to send inside request

    @Override
    protected IResponseHandler<R> getResponseHandler() {
        return responseHandler;
    }

    public AbstractHttpEntity getHttpEntity() {
        StringEntity httpEntity = null;
        try {
            httpEntity = new StringEntity(getMapper().writeValueAsString(getPayload()), UTF_8);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return httpEntity;
    }

}
