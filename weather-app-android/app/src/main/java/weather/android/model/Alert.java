package weather.android.model;

/**
 * Created by Admin on 2016.02.08..
 */
public class Alert {

    private Long id;
    private String city;
    private String email;
    private Integer temperature;

    public Alert(){

    }

    public Long getId() {
        return id;
    }

    public boolean hasId(){
        return id != null;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }
}
