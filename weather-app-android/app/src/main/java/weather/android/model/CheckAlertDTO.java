package weather.android.model;

/**
 * Created by Admin on 2016.02.08..
 */
public class CheckAlertDTO {

    private boolean taken;
    private Alert alert;

    public CheckAlertDTO(){

    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    public Alert getAlert() {
        return alert;
    }

    public void setAlert(Alert alert) {
        this.alert = alert;
    }
}
