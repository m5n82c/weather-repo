package weather.android;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import weather.android.http.AlertResponseHandler;
import weather.android.http.CheckAlertRequest;
import weather.android.http.PostAlertRequest;
import weather.android.http.PutAlertRequest;
import weather.android.http.response.IResponseHandler;
import weather.android.model.Alert;
import weather.android.model.CheckAlertDTO;
import weather.android.util.Util;

/**
 * Created by Admin on 2016.02.08..
 */
public class DialogOnClickListener implements View.OnClickListener, IResponseHandler<CheckAlertDTO> {

    private Context context;

    private TextWatcher watcher;
    private ImageView headerView;
    private TextView statusMessageView;

    private Alert alert;

    public DialogOnClickListener(Context context){
        this.context = context;
        alert = new Alert();
    }

    public void setCity(String city){
        alert.setCity(city);
    }

    @Override
    public void onClick(View v) {
        if (MapsActivity.INVALID_LOCATION.equals(alert.getCity())){
            Toast.makeText(context, "Choose valid location", Toast.LENGTH_SHORT).show();
            return;
        }

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_data);

        headerView = (ImageView)dialog.findViewById(R.id.message_background);
        statusMessageView = (TextView)dialog.findViewById(R.id.status_message);
        statusMessageView.setText("Email and city is required to fill.");

        TextView cityName = (TextView) dialog.findViewById(R.id.city_name);
        cityName.setText(alert.getCity());
        cityName.addTextChangedListener(getWatcher());

        EditText emailInput = (EditText) dialog.findViewById(R.id.email);
        emailInput.addTextChangedListener(getWatcher());

        final TextView temperatureText = (TextView) dialog.findViewById(R.id.temperature);
        final SeekBar temperatureBar = (SeekBar) dialog.findViewById(R.id.seekBar);
        temperatureBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                temperatureText.setText("Temperature: "+(progress-50));
                alert.setTemperature(progress-50);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        temperatureBar.setProgress(50);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alert.hasId()){
                    PutAlertRequest request = new PutAlertRequest(alert, new AlertResponseHandler(dialog));
                    request.execute();
                } else {
                    PostAlertRequest request = new PostAlertRequest(alert, new AlertResponseHandler(dialog));
                    request.execute();
                }
            }
        });

        ((Button)dialog.findViewById(R.id.btn_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private TextWatcher getWatcher(){
        if (watcher == null){
            watcher = new TextWatcher() {

                @Override
                public void afterTextChanged(Editable newEmail) {
                    if (newEmail != null && alert.getCity() != null) {
                        if (!Util.isValidEmail(newEmail.toString())){
                            headerView.setBackgroundColor(Color.parseColor("#f39c12"));
                            statusMessageView.setText("Please type valid email");
                        } else {
                            alert.setEmail(newEmail.toString());
                            CheckAlertRequest request = new CheckAlertRequest(alert, DialogOnClickListener.this);
                            request.execute();
                        }
                    } else {
                        headerView.getBackground().setColorFilter(Color.parseColor("#DA5F6A"), PorterDuff.Mode.DARKEN);
                        statusMessageView.setText("Email and city is required to fill.");
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    // do-nothing
                }

                @Override
                public void onTextChanged(CharSequence newEmail, int start, int before, int count) {
                    // do-nothing
                }
            };
        }
        return watcher;
    }

    @Override
    public void reportAlert(String error) {
        Toast.makeText(context, "Something went wrong. Try again later.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void reportResult(CheckAlertDTO result) {
        if (result.isTaken()){
            headerView.setBackgroundColor(Color.parseColor("#f39c12"));
            this.alert.setId(result.getAlert().getId());
            statusMessageView.setText("Your previous alert will be overridden. Saved temperature: "+result.getAlert().getTemperature());
        } else {
            this.alert.setId(null);
            headerView.setBackgroundColor(Color.parseColor("#2ecc71"));
            statusMessageView.setText("Everything is awesome!");
        }
    }
}
