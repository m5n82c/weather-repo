package weather.android.util;

import android.util.Patterns;

import java.util.regex.Pattern;

/**
 * Created by Admin on 2016.02.08..
 */
public class Util {

    public static boolean isValidEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

}
