package weather.war.dto;

/**
 *
 * @author zsolt.vincze
 */
public class AlertDTO {
    
    private Long id;
    private Integer temperature;
    private String email;
    private String city;
    
    public AlertDTO(){
        
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public boolean hasId(){
        return id != null;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }
    
}
