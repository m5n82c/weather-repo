package weather.war.dto;

import weather.ejb.model.Alert;

/**
 *
 * @author zsolt.vincze
 */
public class CheckDTO {
    
    private boolean taken;
    private Alert alert;
    
    public CheckDTO(){
        
    }
    
    public CheckDTO(Alert alert){
        this.alert = alert;
        this.taken = alert != null;
    }
    
    public CheckDTO(boolean taken){
        this.taken = taken;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    public Alert getAlert() {
        return alert;
    }

    public void setAlert(Alert alert) {
        this.alert = alert;
    }
}
