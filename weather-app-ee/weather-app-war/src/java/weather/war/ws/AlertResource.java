package weather.war.ws;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import weather.ejb.bean.AlertBean;
import weather.ejb.bean.OpenWeatherBean;
import weather.ejb.exception.AlertAlreadyCreatedException;
import weather.ejb.model.Alert;
import weather.ejb.model.OpenWeatherInfo;
import weather.war.dto.AlertDTO;
import weather.war.dto.CheckDTO;

/**
 *
 * @author zsolt.vincze
 */
@Path("/alert")
@Stateless
public class AlertResource {

    @Inject
    private AlertBean alertBean;
    
    @Inject
    private OpenWeatherBean weatherBean;
    
    @GET
    @Path("/{id}/temperature")
    public Response getTemperatureByAlertId(@PathParam("id")Long alertId){
        Integer temperature = alertBean.getTemperatureByAlertId(alertId);
        return Response.ok(temperature).build();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createAlert(AlertDTO dto) throws AlertAlreadyCreatedException{
        if (dto.hasId()){
            throw new AlertAlreadyCreatedException(dto.getId());
        } else {
            Alert checkAlert = alertBean.findAlertByEmailAndCity(dto.getEmail(), dto.getCity());
            if (checkAlert != null){
                dto.setId(checkAlert.getId());
                return editAlert(dto.getId(), dto);
            }
        }
        
        long cityId = weatherBean.resolveCityId(dto.getCity());
        OpenWeatherInfo weatherInfo = new OpenWeatherInfo();
        weatherInfo.setCityId(cityId);
        
        Alert alert = new Alert();
        alert.setCity(dto.getCity());
        alert.setEmail(dto.getEmail());
        alert.setTemperature(dto.getTemperature());
        alert.setWeatherInfo(weatherInfo);
        alertBean.persistAlert(alert);
        return Response.ok(alert).build();
    }
    
    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response editAlert(@PathParam("id") Long alertId, AlertDTO dto){
        Alert alert = alertBean.findAlertById(alertId);
        alert.setTemperature(dto.getTemperature());
        alert.setLastNotificationAt(null);
        alert = alertBean.mergeAlert(alert);
        return Response.ok(alert).build();
    }
    
    @POST
    @Path("/check-alert")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response checkAlertByEmailAndCity(AlertDTO alert){
        Long count = alertBean.countAlertByEmailAndCity(alert.getEmail(), alert.getCity());
        return Response.ok(new CheckDTO(count>0)).build();
    }
    
    @POST
    @Path("/check-and-return-alert")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response checkAndReturnAlertByEmailAndCity(AlertDTO alert){
        Alert entity = alertBean.findAlertByEmailAndCity(alert.getEmail(), alert.getCity());
        return Response.ok(new CheckDTO(entity)).build();
    }
    
    @GET
    @Path("/{id}/unsubscribe")
    public Response unsubscribe(@PathParam("id")Long alertId){
        alertBean.deleteAlert(alertId);
        return Response.ok("You have successfully unsubscribed.").build();
    }
    
}
