package weather.war.ws;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author zsolt.vincze
 */
@javax.ws.rs.ApplicationPath("rest")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(weather.war.exceptionmapper.WeatherAppExceptionMapper.class);
        resources.add(weather.war.ws.AlertResource.class);
        resources.add(weather.war.ws.GenericResource.class);
    }
    
}
