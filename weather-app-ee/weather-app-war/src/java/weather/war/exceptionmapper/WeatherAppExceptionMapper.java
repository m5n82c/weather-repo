package weather.war.exceptionmapper;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import weather.ejb.exception.WeatherAppException;

/**
 *
 * @author Admin
 */
@Provider
public class WeatherAppExceptionMapper implements ExceptionMapper<WeatherAppException>{

    private static final Logger LOG = Logger.getLogger(WeatherAppExceptionMapper.class.getName());
    
    @Override
    public Response toResponse(WeatherAppException exception) {
        LOG.log(Level.INFO, exception.getMessage(), exception);
        return Response.status(Response.Status.BAD_REQUEST).entity(exception.getErrorCode()).build();
    }
    
}
