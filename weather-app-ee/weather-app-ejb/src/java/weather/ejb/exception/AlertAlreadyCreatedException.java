package weather.ejb.exception;
/**
 *
 * @author zsolt.vincze
 */
public class AlertAlreadyCreatedException extends WeatherAppException{

    public AlertAlreadyCreatedException(Long alertId) {
        super("Alert ["+alertId+"] has already created.");
    }

    public AlertAlreadyCreatedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AlertAlreadyCreatedException(Throwable cause) {
        super(cause);
    }

    public AlertAlreadyCreatedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public String getErrorCode() {
        return "alert.already-exists";
    }

    
}
