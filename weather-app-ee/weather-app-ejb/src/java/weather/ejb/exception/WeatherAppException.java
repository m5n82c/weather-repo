package weather.ejb.exception;

/**
 *
 * @author zsolt.vincze
 */
public abstract class WeatherAppException extends Exception{
    
    public abstract String getErrorCode();
    
    public WeatherAppException() {
    }

    public WeatherAppException(String message) {
        super(message);
    }

    public WeatherAppException(String message, Throwable cause) {
        super(message, cause);
    }

    public WeatherAppException(Throwable cause) {
        super(cause);
    }

    public WeatherAppException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
