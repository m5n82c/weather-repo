package weather.ejb.model;

import java.util.Date;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author zsolt.vincze
 */
@Entity
@Table(name="weather_alert")
@NamedQueries({
    @NamedQuery(name = "Alert.findTemperatureById", query = "SELECT a.temperature FROM Alert a WHERE a.id = :alertId"),
    @NamedQuery(name = "Alert.findByEmailAndCity", query = "SELECT a FROM Alert a WHERE a.email = :email AND a.city = :city"),
    @NamedQuery(name = "Alert.countByEmailAndCity", query = "SELECT COUNT(a.id) FROM Alert a WHERE a.email = :email AND a.city = :city")}) 
public class Alert implements java.io.Serializable {
    
    @Id @GeneratedValue
    private Long id;
    private String city;
    @Column(name = "email_address")
    private String email;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_notification_date")
    private Date lastNotificationAt;
    private Integer temperature;
    
    @OneToOne(fetch=FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name="weather_info_id")
    private OpenWeatherInfo weatherInfo;
    
    public Alert(){
        
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    } 

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getLastNotificationAt() {
        return lastNotificationAt;
    }

    public void setLastNotificationAt(Date lastNotificationAt) {
        this.lastNotificationAt = lastNotificationAt;
    }

    public Integer getTemperature() {
        return temperature;
    }

    public void setTemperature(Integer temperature) {
        this.temperature = temperature;
    }

    @XmlTransient
    public OpenWeatherInfo getWeatherInfo() {
        return weatherInfo;
    }

    public void setWeatherInfo(OpenWeatherInfo weatherInfo) {
        if (weatherInfo.hasAlert()){
            weatherInfo.getAlert().setWeatherInfo(null);
        }
        this.weatherInfo = weatherInfo;
        this.weatherInfo.setAlert(this);
    }
    
    @Override
    public int hashCode() { 
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Alert other = (Alert) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
        return "Alert ["+id+"]";
    }
    
}
