package weather.ejb.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author zsolt.vincze
 */
@Entity
@Table(name = "open_weather_info")
public class OpenWeatherInfo implements java.io.Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private Long cityId;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "weatherInfo")
    private Alert alert;
    
    public OpenWeatherInfo(){
        
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Alert getAlert() {
        return alert;
    }

    public void setAlert(Alert alert) {
        this.alert = alert;
    }
    
    public boolean hasAlert(){
        return alert != null;
    }
    
    @Override
    public String toString(){
        return "OpenWeatherInfo ["+id+"]";
    }
}
