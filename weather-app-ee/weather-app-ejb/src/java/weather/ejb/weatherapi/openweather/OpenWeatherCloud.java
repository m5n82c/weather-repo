package weather.ejb.weatherapi.openweather;

/**
 *
 * @author zsolt.vincze
 */
public class OpenWeatherCloud {
    
    private double all;
    
    public OpenWeatherCloud(){
        
    }

    public double getAll() {
        return all;
    }

    public void setAll(double all) {
        this.all = all;
    }
    
}
