package weather.ejb.weatherapi.openweather;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author zsolt.vincze
 */
public class OpenWeatherList {
    
    private Integer cnt;
    private List<OpenWeather> list;
    private Map<Long, OpenWeather> map;
    
    public OpenWeatherList(){
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public List<OpenWeather> getList() {
        return list;
    }

    public void setList(List<OpenWeather> list) {
        this.list = list;
    }
    
    public OpenWeather getWeatherByCityId(Long cityId){
        if (map != null){
            return map.get(cityId);
        }
        return null;
    }
    
    public void initMap(){
        map = new HashMap<>();
        for(OpenWeather weather : list){
            map.put(weather.getId(), weather);
        }
    }
}
