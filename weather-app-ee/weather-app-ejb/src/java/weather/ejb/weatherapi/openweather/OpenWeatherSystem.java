package weather.ejb.weatherapi.openweather;

/**
 *
 * @author zsolt.vincze
 */
public class OpenWeatherSystem {
 
    private double message;
    private String country;
    private double sunrise;
    private double sunset;
    
    public OpenWeatherSystem(){
        
    }

    public double getMessage() {
        return message;
    }

    public void setMessage(double message) {
        this.message = message;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getSunrise() {
        return sunrise;
    }

    public void setSunrise(double sunrise) {
        this.sunrise = sunrise;
    }

    public double getSunset() {
        return sunset;
    }

    public void setSunset(double sunset) {
        this.sunset = sunset;
    }
    
}