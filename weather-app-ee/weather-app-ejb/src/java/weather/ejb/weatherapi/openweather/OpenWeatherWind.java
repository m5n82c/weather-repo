package weather.ejb.weatherapi.openweather;

/**
 *
 * @author zsolt.vincze
 */
public class OpenWeatherWind {
    
    private double speed;
    private double deg;
    private double var_beg;
    private double var_end;
    
    public OpenWeatherWind(){
        
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getDeg() {
        return deg;
    }

    public void setDeg(double deg) {
        this.deg = deg;
    }

    public double getVar_beg() {
        return var_beg;
    }

    public void setVar_beg(double var_beg) {
        this.var_beg = var_beg;
    }

    public double getVar_end() {
        return var_end;
    }

    public void setVar_end(double var_end) {
        this.var_end = var_end;
    }
    
}
