package weather.ejb.weatherapi.openweather;

/**
 *
 * @author zsolt.vincze
 */
public class OpenWeatherCoord {
    
    private double lon;
    private double lat;
    
    public OpenWeatherCoord(){
        
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
