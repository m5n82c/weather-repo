package weather.ejb.weatherapi.openweather;

import java.util.List;

/**
 *
 * @author zsolt.vincze
 */
public class OpenWeather {
    
    private OpenWeatherCoord coord;
    private List<OpenWeatherCondition> weather;
    private String base;
    private OpenWeatherMain main;
    private OpenWeatherWind wind;
    private OpenWeatherCloud clouds;
    private double dt;
    private Long id;
    private String name;
    private double cod;
    private OpenWeatherSystem sys;
    
    public OpenWeather(){
        
    }

    public OpenWeatherCoord getCoord() {
        return coord;
    }

    public void setCoord(OpenWeatherCoord coord) {
        this.coord = coord;
    }

    public List<OpenWeatherCondition> getWeather() {
        return weather;
    }

    public void setWeather(List<OpenWeatherCondition> weather) {
        this.weather = weather;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public OpenWeatherMain getMain() {
        return main;
    }

    public void setMain(OpenWeatherMain main) {
        this.main = main;
    }

    public OpenWeatherWind getWind() {
        return wind;
    }

    public void setWind(OpenWeatherWind wind) {
        this.wind = wind;
    }

    public OpenWeatherCloud getClouds() {
        return clouds;
    }

    public void setClouds(OpenWeatherCloud clouds) {
        this.clouds = clouds;
    }

    public double getDt() {
        return dt;
    }

    public void setDt(double dt) {
        this.dt = dt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCod() {
        return cod;
    }

    public void setCod(double cod) {
        this.cod = cod;
    }

    public OpenWeatherSystem getSys() {
        return sys;
    }

    public void setSys(OpenWeatherSystem sys) {
        this.sys = sys;
    }
    
    
    
}
