package weather.ejb.bean;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import weather.ejb.weatherapi.openweather.OpenWeather;
import weather.ejb.weatherapi.openweather.OpenWeatherList;

/**
 *
 * @author zsolt.vincze
 */
@Stateless
public class OpenWeatherBean {
    
    private final static String BASE_URL = "http://api.openweathermap.org/data/2.5";
    private final static String PARAM_NAME_Q = "q";
    private final static String PARAM_NAME_ID = "id";
    private final static String PARAM_NAME_APP_ID = "appid";
    private final static String API_KEY = "f48b0bacbb096d5adb43d570d4f702b7";
    
    private Client client = null;
    
    @PostConstruct
    public void init(){
        client = ClientBuilder.newClient();
    }
    
    @PreDestroy
    public void destroy(){
        client.close();
    }
    public long resolveCityId(String cityName){
        WebTarget resource = client.target(BASE_URL+"/weather")
                .queryParam(PARAM_NAME_Q, cityName).queryParam(PARAM_NAME_APP_ID, API_KEY);
        OpenWeather weather = resource.request(MediaType.APPLICATION_JSON).get(OpenWeather.class);
        return weather.getId();
    }
    
    public OpenWeatherList resolveCities(List<Long> cityIds){
        if (cityIds.isEmpty()){
            return null;
        }
        StringBuilder cityParam = new StringBuilder();
        for(Long cityId : cityIds){
            cityParam.append(cityId).append(",");
        }
        cityParam.replace(cityParam.lastIndexOf(","), cityParam.length(), "");
        WebTarget resource = client.target(BASE_URL+"/group")
                .queryParam(PARAM_NAME_ID, cityParam.toString()).queryParam(PARAM_NAME_APP_ID, API_KEY);
        OpenWeatherList list = resource.request(MediaType.APPLICATION_JSON).get(OpenWeatherList.class);
        list.initMap();
        return list;
    }
    
}
