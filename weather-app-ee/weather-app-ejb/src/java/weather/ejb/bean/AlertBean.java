package weather.ejb.bean;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import weather.ejb.model.Alert;
import weather.ejb.weatherapi.openweather.OpenWeather;
import weather.ejb.weatherapi.openweather.OpenWeatherList;

/**
 *
 * @author zsolt.vincze
 */
@Stateless
public class AlertBean {

    private static final Logger LOG = Logger.getLogger(AlertBean.class.getName());

    @PersistenceContext(unitName = "weather-PU")
    private EntityManager entityManager;

    public Alert findAlertById(Long alertId) {
        return entityManager.find(Alert.class, alertId);
    }

    public Alert persistAlert(Alert alert) {
        entityManager.persist(alert);
        return alert;
    }

    public Alert mergeAlert(Alert alert) {
        return entityManager.merge(alert);
    }
    
    public void deleteAlert(Long alertId){
        entityManager.remove(findAlertById(alertId));
    }

    public Long countAlertByEmailAndCity(String email, String city) {
        try {
            Query query = entityManager.createNamedQuery("Alert.countByEmailAndCity");
            query.setParameter("email", email);
            query.setParameter("city", city);
            return (Long) query.getSingleResult();
        } catch (NoResultException e) {
            LOG.log(Level.INFO, e.getMessage());
            return 0L;
        }
    }

    public Alert findAlertByEmailAndCity(String email, String city) {
        try {
            TypedQuery<Alert> query = entityManager.createNamedQuery("Alert.findByEmailAndCity", Alert.class);
            query.setParameter("email", email);
            query.setParameter("city", city);
            return query.getSingleResult();
        } catch (NoResultException e) {
            LOG.log(Level.INFO, e.getMessage());
            return null;
        }
    }

    public Integer getTemperatureByAlertId(Long alertId) {
        try {
            Query query = entityManager.createNamedQuery("Alert.findTemperatureById");
            query.setParameter("alertId", alertId);
            return (Integer) query.getSingleResult();
        } catch (NoResultException e) {
            LOG.log(Level.INFO, e.getMessage());
            return null;
        }
    }

    public List<Long> getCityIdsToNotify() {
        TypedQuery<Long> query = entityManager.createQuery("SELECT distinct(a.weatherInfo.cityId) FROM Alert a WHERE a.lastNotificationAt IS NULL OR a.lastNotificationAt < :date", Long.class);
        query.setParameter("date", getYesterdayAsDate());
        return query.getResultList();
    }

    public List<Alert> findAlertByWeathers(OpenWeatherList weathers) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Alert> query = builder.createQuery(Alert.class);
        Root<Alert> root = query.from(Alert.class);
        
        Predicate[] orPredicates = new Predicate[weathers.getCnt()];
        int i = 0;
        for(OpenWeather weather : weathers.getList()){
            orPredicates[i] = 
                builder.and(
                    builder.equal(root.get("weatherInfo").get("cityId"), weather.getId()),
                    builder.lessThanOrEqualTo(root.get("temperature"), weather.getMain().getTempInCelsius()),
                    builder.or(
                            builder.isNull(root.get("lastNotificationAt")),
                            builder.lessThanOrEqualTo(root.get("lastNotificationAt"), getYesterdayAsDate()))
                    );
            ++i;
        }
        
        query.select(root).where(builder.or(orPredicates));
        return entityManager.createQuery(query).getResultList();
    }

    private Date getYesterdayAsDate() {
        LocalDateTime yesterday = LocalDateTime.now().minusDays(1);
        return new Date(Timestamp.valueOf(yesterday).getTime());
    }
}
