package weather.ejb.bean;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author zsolt.vincze
 */
@Stateless
public class TimerBean {

    @Inject
    private EmailBean emailBean;
    
    @Schedule(hour = "*", minute = "*/15")
    public void check() {
        emailBean.checkAlerts();
    }


}
