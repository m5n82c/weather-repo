package weather.ejb.bean;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import static javax.ejb.TransactionAttributeType.REQUIRES_NEW;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import weather.ejb.model.Alert;
import weather.ejb.weatherapi.openweather.OpenWeather;
import weather.ejb.weatherapi.openweather.OpenWeatherCondition;
import weather.ejb.weatherapi.openweather.OpenWeatherList;

/**
 *
 * @author zsolt.vincze
 */
@Stateless
public class EmailBean {
     private static final Logger LOG = Logger.getLogger(EmailBean.class.getName());

    @Resource(lookup = "mail/weather")
    private Session mailSession;

    @Inject
    private AlertBean alertBean;

    @Inject
    private OpenWeatherBean openWeatherBean;
    
    @TransactionAttribute(REQUIRES_NEW)
    public void checkAlerts() {
        List<Long> cityIds = alertBean.getCityIdsToNotify();
        OpenWeatherList weatherList = openWeatherBean.resolveCities(cityIds);
        if (weatherList != null) {
            List<Alert> alerts = alertBean.findAlertByWeathers(weatherList);
            for (Alert alert : alerts) {
                sendEmail(alert, weatherList.getWeatherByCityId(alert.getWeatherInfo().getCityId()));
            }
        }
    }

//    @Asynchronous
    private void sendEmail(Alert alert, OpenWeather weather) {
        try {
            Message message = new MimeMessage(mailSession);
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(alert.getEmail()));
            message.setFrom(InternetAddress.parse("info@weather.app")[0]);
            message.setSubject("Temperature in " + alert.getCity() + " is over " + alert.getTemperature() + "°C.");
            message.setText(buildWeatherText(alert, weather));
            Transport.send(message);
            alert.setLastNotificationAt(new Date());
        } catch (MessagingException ex) {
            LOG.log(Level.INFO, ex.getMessage());
        }
    }

    private final static String NEW_LINE = System.getProperty("line.separator");

    private String buildWeatherText(Alert alert, OpenWeather weather) {
        StringBuilder weatherBuilder = new StringBuilder();
        if (weather != null) {
            weatherBuilder.append(weather.getName()).append(NEW_LINE)
                    .append(NEW_LINE).append("Temperature: ").append(weather.getMain().getTempInCelsius()).append(NEW_LINE)
                    .append("Pressure: ").append(weather.getMain().getPressure()).append(NEW_LINE)
                    .append("Humidity: ").append(weather.getMain().getHumidity()).append(NEW_LINE)
                    .append(NEW_LINE).append("*** Location ***").append(NEW_LINE)
                    .append("Latitude: ").append(weather.getCoord().getLat()).append(NEW_LINE)
                    .append("Longitude: ").append(weather.getCoord().getLon()).append(NEW_LINE)
                    .append(NEW_LINE).append("*** Wind ***").append(NEW_LINE)
                    .append("Deg: ").append(weather.getWind().getDeg()).append(NEW_LINE)
                    .append("Speed: ").append(weather.getWind().getSpeed()).append(NEW_LINE)
                    .append(NEW_LINE).append("*** Conditions ***").append(NEW_LINE);
            for (OpenWeatherCondition condition : weather.getWeather()) {
                weatherBuilder.append(condition.getDescription()).append(NEW_LINE);
            }
            weatherBuilder.append(NEW_LINE);
        }
        weatherBuilder.append("Please do not reply.").append(NEW_LINE);
        weatherBuilder.append("To unsubscribe open: http://198.199.127.60:8080/weather-app-war/rest/alert/").append(alert.getId()).append("/unsubscribe").append(NEW_LINE);
        weatherBuilder.append("Open this link will remove your alert.");
        return weatherBuilder.toString();
    }
}
